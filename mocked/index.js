const home = require('./pages/home');
const blog = require('./pages/blog');
const contact = require('./pages/contact');

module.exports = {
  home,
  blog,
  contact
};
