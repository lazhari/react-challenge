const faker = require('faker');
const componentGenerator = require('../componentGenerator')();
module.exports = {
  type: 'Home',
  title: faker.hacker.phrase(),
  meta_data: {
    created_at: faker.date.past(),
    title: faker.hacker.phrase(),
    description: faker.lorem.sentence()
  },
  components: [componentGenerator.jumbotron(), componentGenerator.productList()]
};
