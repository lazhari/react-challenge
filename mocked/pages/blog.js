const faker = require('faker');
const componentGenerator = require('../componentGenerator')();
module.exports = {
  type: 'Blog',
  title: 'Blog',
  meta_data: {
    created_at: faker.date.past(),
    title: faker.hacker.phrase(),
    description: faker.lorem.sentence()
  },
  components: [componentGenerator.articleList()]
};
