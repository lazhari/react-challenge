const faker = require('faker');
const componentGenerator = require('../componentGenerator')();
module.exports = {
  type: 'Contact',
  title: faker.hacker.phrase(),
  meta_data: {
    created_at: faker.date.past(),
    title: faker.hacker.phrase(),
    description: faker.lorem.sentence()
  },
  components: [componentGenerator.jumbotron()],
  form: {
    fields: [
      {
        label: 'Name',
        placeholder: 'You Name',
        type: 'text',
        errors: [
          {
            required: true,
            message: 'The Name is required'
          }
        ]
      },
      {
        label: 'E-mail',
        placeholder: 'E-mail',
        text: 'text',
        errors: [
          {
            required: true,
            message: 'The Email is required'
          },
          {
            email: true,
            message: 'The email is not valid'
          }
        ]
      },
      {
        label: 'Subject',
        placeholder: 'Subject',
        type: 'select',
        options: ['Services', 'Support', 'Sales', 'Marketing'],
        errors: [
          {
            required: true,
            message: 'The subject is required'
          }
        ]
      },
      {
        label: 'Message',
        placeholder: 'Message',
        type: 'textarea',
        errors: [
          {
            required: true,
            message: 'The message is required'
          }
        ]
      }
    ],
    actions: {
      submitButton: {
        label: 'Send',
        type: 'success'
      },
      resetButton: {
        label: 'Cancel',
        type: 'warning'
      }
    }
  }
};
