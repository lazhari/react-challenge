const faker = require('faker');

function componentGenerator() {
  const generateProducts = () => {
    const products = [];
    for (let i = 0; i < 3; i++) {
      products.push({
        image: faker.random.image(),
        title: faker.commerce.productName(),
        description: faker.random.words(),
        url: faker.internet.url()
      });
    }
    return products;
  };

  const generateArticles = () => {
    const articles = [];
    for (let i = 0; i < 20; i++) {
      articles.push({
        _id: faker.random.uuid(),
        title: faker.commerce.productName(),
        description: faker.lorem.sentences(),
        author: {
          name: faker.internet.userName(),
          avatar: faker.internet.avatar()
        }
      });
    }
    return articles;
  };

  return {
    jumbotron: () => {
      faker.seed(Math.random());
      const hasButton = faker.random.boolean();
      const jumbotron = {
        hasButton,
        type: 'Jumbotron',
        title: faker.lorem.words(),
        subTitle: faker.lorem.sentence(),
        description: faker.lorem.sentences(),
        background_color: faker.internet.color()
      };
      if (hasButton) {
        const external = faker.random.boolean();
        jumbotron.button = {
          external,
          title: faker.hacker.verb()
        };
        if (external) {
          jumbotron.button.link = faker.internet.url();
        }
      }
      return jumbotron;
    },
    productList: () => ({
      type: 'ProductList',
      title: faker.commerce.productAdjective(),
      products: generateProducts()
    }),
    articleList: () => ({
      type: 'ArticleList',
      title: 'Articles',
      articles: generateArticles()
    })
  };
}

module.exports = componentGenerator;
