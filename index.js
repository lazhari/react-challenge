const express = require('express');

const app = express();

app.use(require('./routes'));

app.get('/', (req, res) => {
  return res.send({
    version: '0.1.0',
    date: new Date()
  });
});

app.listen(process.env.PORT || 3500, error => {
  if (error) {
    console.warn(`Something is broken ${error.message}`);
  }
  console.info(
    `The magic happen at http://localhost:${process.env.PORT || 3500}`
  );
});
