# React Contest

> React Contest based on a mocked CMS micro api and React.

## The Goal of challenge

The challenge goal is been able to understand and consume the mocked API, through a client React app.

The API return resources such pages:

- The home page: [http://localhost:3500/api/v1/contents/](http://localhost:3500/api/v1/contents/)
- The blog page: [http://localhost:3500/api/v1/contents/blog](http://localhost:3500/api/v1/contents/blog)
- The contact page: [http://localhost:3500/api/v1/contents/contact](http://localhost:3500/api/v1/contents/contact)

Each page composed of:

- type: The page type
- title: the page title
- meta_data: of the page
- components: list of component
  - type: the component type
  - title: the title for the component
  - And other unique attributes
- form: for the contact page

**Hint**: The goals is to resolve each page, and their components, inside your React app.

For example if we look to Jumbotron component, the data returned from the api:

```json
{
  "title": "ipsam qui delectus",
  "subTitle": "Ut voluptas rerum possimus aut eos laborum possimus.",
  "description": "Maxime voluptas possimus dicta quae dolor vitae iusto. Eum dolor exercitationem odit deleniti temporibus reiciendis eius. Nostrum maxime consectetur vero omnis eveniet dolor. Sed nesciunt accusamus ipsam est aut mollitia eaque sit.",
  "hasButton": true,
  "button": {
    "title": "generate",
    "external": false,
    "link": "http://mariane.info"
  },
  "background_color": "#6c1a21"
}
```

And final result should be similar to this:

![Jumbotron Component](screenshots/Jumbotron.png)

## Requirements

- The client routes should match the page routes from the API, from example `/blog` should returns the blog page and `/contact` resolve the contact page. The idea behind this resolver should be dynamic. So if we add a new page such as `/about-us` we don't need to make any updates on React side.
- The components contains dynamic data so you need to think in generic solution for that.

## Technical Requirements:

- You need to make a clean structure.
- Respecting the main principals of programming such as DRY, YAGNI, KISS (those isn't Pokemons)
- Use a state management package, just pick what you want (Redux, Mobx) or anything else,
- Set up prettier with eslint on client folder
- Write some clean commit messages [https://chris.beams.io/posts/git-commit/](https://chris.beams.io/posts/git-commit/)

## Bonus:

- Writing unit testing for the application with Jest and what ever testing library for react
- Writing integrations testing with puppeteer.

## Start the environment:

### Project Requirements

You need to ensure, that you machine contains:

- Node.js > 8.16
- npm

### Install the dependencies

```bash
cd <Project_Path>
yarn or npm install
# to install the client dependencies
cd client
yarn
```

### Start a dev environment

You can start the application on dev environment, by executing the command below:

```
npm run dev
```

Try to visit [http://localhost:3000/](http://localhost:3000/) and for the API, you can find that on [http://localhost:3500/api/v1/contents](http://localhost:3500/api/v1/contents)
