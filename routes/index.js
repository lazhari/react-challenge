const { Router } = require('express');

const apiRoute = Router();

const mocked = require('../mocked');

const API_ROOT = '/api/v1';

apiRoute
  .get(`${API_ROOT}/contents`, (req, res) => {
    return res.send(mocked.home);
  })
  .get(`${API_ROOT}/contents/blog`, (req, res) => {
    return res.send(mocked.blog);
  })
  .get(`${API_ROOT}/contents/contact`, (req, res) => {
    return res.send(mocked.contact);
  });

module.exports = apiRoute;
